import { useState } from 'react'
import Unity, { UnityContext } from "react-unity-webgl";
import './App.css';

const projectName = 'fps'

const unityContext = new UnityContext({
  loaderUrl: `./Build/${projectName}.loader.js`,
  dataUrl: `./Build/${projectName}.data`,
  frameworkUrl: `./Build/${projectName}.framework.js`,
  codeUrl: `./Build/${projectName}.wasm`,
});

/**
 * App
 * ===
 */
function App() {

  // State

  const [command, setCommand] = useState("")
  const [fps, setFps] = useState(0)

  // Unity Callback

  unityContext.on('SendFps', (fps) => {
    setFps(Number(fps ?? 0).toFixed(2));
  })

  // Actions

  const sendCommand = (actionName) => {
    unityContext.send("Cube", `RunAnimation`, actionName);
    setCommand(actionName);
  }

  // Handlers

  const handleGetFps = () => {
    const unityFps = unityContext.send("Cube", `RequestFps`);
  }

  return (
    <div className="App">
      <header>
        <h1>React Unity POC</h1>
      </header>
      <main className="unity">
        <h4>Unity WebGL:</h4>
        <Unity unityContext={unityContext} style={{ width: '800px', height: '480px' }} />
      </main>
      <main>
        <div className="control">
          <h4>React controls:</h4>
          <button onClick={() => sendCommand("Iddle")}>Iddle</button>
          <button onClick={() => sendCommand("Jump")}>Jump</button>
          <button onClick={() => sendCommand("Roll")}>Roll</button>
        </div>
        <div className="command-log">
          <h4>Last Command:</h4>
          <strong>{command}</strong>
        </div>
        <div className="command-log">
          <h4>FPS rate:</h4>
          <strong>{fps}</strong><br /><br />
          <button onClick={() => handleGetFps()}>Get FPS rate</button>
        </div>
      </main>
    </div>
  );
}

export default App;
